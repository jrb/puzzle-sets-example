# Example Puzzle Set for GNOME Crosswords

This is an example PuzzleSet for GNOME Crosswords. It can be used to create an additional source of puzzles for the game.

## PuzzleSet types

There are two types of PuzzleSets:
* **Downloader**: Contains a helper application that can be used to
  download puzzles from a web source.
* **Resource**: Self-contained puzzle resouce with the puzzles inside.

In theory, it's possible to have both of these in the same PuzzleSet but in practice that's a poor user experience.

This example Puzzle Set can be used to generate both types.

## Documentation
Information for working with puzzle sets can be read in the main app documentation:
* https://gitlab.gnome.org/jrb/crosswords/-/blob/master/docs/puzzle-set-resource.md
* https://gitlab.gnome.org/jrb/crosswords/-/blob/master/docs/puzzle-downloader.md

## Downloader

If you are writing a downloader, you should test your output by loading it in Crosswords. There's also great documentation on the ipuz file format at http://ipuz.org/

## How to use this repo

* Make a copy of this repo into its own directory.
* Search for all examples of "EDIT:" in the directory. These should be removed, and replaced with your own values. Iterate until they're all gone.

```grep -rnw . -e "EDIT:"  #NOT THIS ONE```

* Make sure you choose the correct puzzle.config file in `puzzle-sets/examples/` and rename it to puzzle.config.
* Rename all files prefixed as org.domain with your application name and ID.
* Be sure to rename the directory in `puzzle-sets/`
* Test it by running `flatpak-builder  --force-clean _flatpak/ org.domain.Puzzlesets.example.json --user --install`
* Upload it to flathub once you've gotten it working. You'll have to change the flatpak manifest to refer to a git repo rather than local files.
