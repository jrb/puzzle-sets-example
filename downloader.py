#!/usr/bin/python3
import json
import requests
import sys

datasource = "https://example.com/crosswords/%d"


def build_grid(width, height):
    grid = []
    for i in range (0, height):
        row = [ None ] * width
        grid.append (row)
    return grid

def get_data(puzzle_id):
    # Do something with this
    # url = datasource % puzzle_id
    # page = requests.get(url)
    return None

def get_title(data):
    return "Example Title"

def get_author(data):
    return "J. A. Random"

def get_dimensions(data):
    return dict(width=15, height=15)

def get_board(width, height, data):
    grid = build_grid(width, height)
    for i in range (0, width):
        for j in range (0, height):
            grid[j][i] = "#"
    return grid

def get_solution(width, height, data):
    grid = build_grid(width, height)
    for i in range (0, width):
        for j in range (0, height):
            grid[j][i] = "#"
    return grid

def get_clues(data):
    return dict (Across=[ ["1", "One across"] ],
                 Down=[ ["1", "One down" ]])

def get_puzzle(puzzle_id):
    data = get_data (puzzle_id)
    puzzle = {}
    puzzle["origin"] = "example-downloader"
    puzzle["version"] = "http://ipuz.org/v2"
    puzzle["kind"] = ["http://ipuz.org/crossword"]
    puzzle["showenumerations"] = False
    puzzle["block"] = "#"

    puzzle["title"] = get_title (data)
    dimensions = get_dimensions (data)
    puzzle["dimensions"] = dimensions
    puzzle["puzzle"] = get_board (dimensions['width'], dimensions['height'], data)
    puzzle["solution"] = get_solution (dimensions['width'], dimensions['height'], data)
    puzzle["clues"] = get_clues (data)
    # Optional fields
    # puzzle["copyright"] = get_copyright (data)
    # puzzle["editor"] = get_editor (data)
    # puzzle["notes"] = get_notes (data)
    return puzzle

if __name__ == '__main__':
    puzzle_id = int (sys.argv[1])
    if puzzle_id < 100:
        # All error messages need to go to stderr
        print ("Invalid puzzle ID: %d" % puzzle_id, file=sys.stderr)
        sys.exit (1)
    puzzle = get_puzzle (puzzle_id)
    json.dump (puzzle, sys.stdout)
